#!/bin/sh
# 19-07-2016
# Konrad J. Debski

# path to script
soft_path='/home/seventm/BUCKET_PROJECTS/DNase'

# sorted bam file for each sample
# sample_name:files_with_reads.bam
# separated by conditions
gcm_files='GCM1:reads/GCM_1_rev.bam,GCM2:reads/GCM_2_rev.bam,GCM3:reads/GCM_3_rev.bam'
lps_files='LPS1:reads/LPS_1_rev.bam,LPS2:reads/LPS_2_rev.bam,LPS3:reads/LPS_3_rev.bam'
untr_files='UNTR1:reads/UNTR_1_rev.bam,UNTR2:reads/UNTR_2_rev.bam,UNTR3:reads/UNTR_3_rev.bam'

# joined input
infiles="$gcm_files,$lps_files,$untr_files"

echo $infiles

# from which end dnase cuts should be counted <5 or 3>
end_mode='5'

# -r regions file
# -s segments file
# -e end of dnase cut


outfile_1='dnase_C3R2S0_S7329'
regfile_1='dnase2_REGIONS_C3R2S0_19072016043244607329.brf'
segfile_1='dnase2_SEGMENTS_C3R2S0_19072016043244607329.brf'
python $soft_path/countDNaseCuts.py -i $infiles -e $end_mode -o $outfile_1 -s $segfile_1 -r $regfile_1


outfile_2='dnase_C3RAS0_S5110'
regfile_2='dnase2_REGIONS_C3RAS0_19072016043152575110.brf'
segfile_2='dnase2_SEGMENTS_C3RAS0_19072016043152575110.brf'
python $soft_path/countDNaseCuts.py -i $infiles -e $end_mode -o $outfile_2 -s $segfile_2 -r $regfile_2


