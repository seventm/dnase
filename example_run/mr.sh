#!/bin/bash

# Analysis 19-07-2016
# Konrad J. Debski
# script path
soft_path='/home/seventm/BUCKET_PROJECTS/DNase'

# hotspot output for each condition
gcm_files='hotspots/GCM_1_REV.hot,hotspots/GCM_2_REV.hot,hotspots/GCM_3_REV.hot'
lps_files='hotspots/LPS_1_REV.hot,hotspots/LPS_2_REV.hot,hotspots/LPS_3_REV.hot'
untr_files='hotspots/UNTR_1_REV.hot,hotspots/UNTR_2_REV.hot,hotspots/UNTR_3_REV.hot'

# input for makeRegions - conditions separated with semicolons
infiles="$gcm_files;$lps_files;$untr_files"

echo $infiles
# output prefix
outfile='dnase2'

# -g returns also region file
# -r n replicates per condition per region
# ------|         |---------- # R1
# ---|        |-------------- # R2
# -------|              |---- # R3
# ================================
# -------|    |-------------- # Results when consensus region is a common part of all replicates
# ------|         |---------- # Results when consensus region is a common part of two replicates (-r 2)

# common for all replicates in condition
python $soft_path/makeRegions.py -i $infiles -o $outfile -g
# common for two replicates in condition
python $soft_path/makeRegions.py -i $infiles -o $outfile -g -r 2
