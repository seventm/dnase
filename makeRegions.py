#!/usr/bin/python
import pybedtools
import sys
from sys import stdout as p
import getopt
from os.path import splitext
import datetime

def main(argv):
	
	# get arguments
	param      = getoptions(argv)
	min_rep    = param['n_replicates']
	min_cond   = param['n_conditions']
	outputfile = param['outputfile']
	return_reg = param['regions']
	
	# parse files
	condition_files = get_files(param['inputfile'])
	
	S_NC = str(len(condition_files))
	S_MR = str(min_rep)
	S_RS = str(min_cond)
	line_len = 70
	
	line(line_len)
	print '{:70s}'.format("Parameters: ")
	line(line_len)
	print '{:45s}{:>5s}'.format("Number of conditions: ", S_NC)
	if min_rep == -9:
		min_rep_nfo = 'all'
		S_MR = 'A'
	else:
		min_rep_nfo = str(min_rep)
		
	signature = 'C' + S_NC + 'R' + S_MR + 'S' + S_RS
	signature_seg  = 'SEGMENTS_' + signature
	signature_reg  = 'REGIONS_' + signature
	ts = timestamp()
	outputfile_seg = make_filename(outputfile, signature_seg, ts)
	outputfile_reg = make_filename(outputfile, signature_reg, ts)
	
	print '{:45s}{:>5s}'.format("Number of replicates per condition region: ",  min_rep_nfo)
	print '{:45s}{:>5s}'.format("Number of conditions per region segment: ", S_RS)
	line(line_len)
	print '{:20s} {:>30s}'.format("Output file name:", outputfile)
	print '{:20s} {:>30s}'.format("Output file name [segments]:", outputfile_seg)
	print '{:20s} {:>30s}'.format("Output file name [regions]:", outputfile_reg)
	print '{:20s} {:>30s}'.format("Timestamp:", ts)
	line(line_len)
	
	nfo('Uploading bedfiles ')
	# read bed files per each condition
	conditions = []
	for cfl in condition_files:
		conditions.append(read_bed2list(cfl))
	done()
	
	raw_reg_sum = raw_summary(conditions)
	
	
	nfo('Generating regions per condition ')
	# obtain regions for each condition
	cond_segments = []
	for cnd in conditions:
		cond_segments.append(get_intersections(cnd, min_rep))
	
	raw_reg_sum['common'] = common_summary(cond_segments)
	done()
	
	nfo('Generating consensus segments ')
	# obtain consensus segments
	segments  = get_intersections(cond_segments, min_cond)
	done()
	
	nfo('Generating consensus regions ')
	# obtain consensus regions
	regions = segments.merge()
	done()
	
	nfo('Adding region ids ')
	# add regions id
	max_id, regions = add_indexes(regions)
	done()	
	# add region length
	nfo('Adding regions length ')
	#regions = add_region_len_and_coords(regions)
	regions = add_region_length(regions)
	n_sum_reg = [int(r[4]) for r in regions]
	done()
	
	# saving regions
	if return_reg:
		nfo('Saving regions')
		tmp_regions = remove_reg_coords(regions)
		tmp_regions.saveas(outputfile_reg)
		done()
	
	nfo('Adding segments id ')
	# add segment id and unique segment id in each region
	segments_regions = regions.intersect(segments)
	segments_regions = add_index_segments(segments_regions, max_id)
	done()
	
	nfo('Adding segments length ')
	# add segments length		
	segments_regions = add_seq_length(segments_regions)
	done()
	n_segments = segments_regions.count()
	nfo('Saving segments ')
	# save segmets with information about segments
	segments_regions.saveas(outputfile_seg)
	done()
	
	#n_sum_seg = [int(r[8]) for r in segments_regions]
	n_sum_seg = [int(r[5]) for r in segments_regions]
	#n_sum_len = [int(r[10]) for r in segments_regions]
	n_sum_len = [int(r[7]) for r in segments_regions]
	
	
	line(70)	
	print '{:70s}'.format("Summary: ")
	line(70)
	print '{:40s}{:>10s}'.format("Number of defined regions: ", str(max_id))
	print '{:40s}{:>10s}'.format("Number of defined segments: ", str(n_segments))
	print '{:40s}{:>10s}'.format("Maximal number of segments: ", str(max(n_sum_seg)))
	print '{:40s}{:>10s}'.format("Minimal region length: ", str(min(n_sum_reg)))
	print '{:40s}{:>10s}'.format("Maximal region length: ", str(max(n_sum_reg)))
	print '{:40s}{:>10s}'.format("Minimal segment length: ", str(min(n_sum_len)))
	print '{:40s}{:>10s}'.format("Maximal segment length: ", str(max(n_sum_len)))
	print '{:40s}{:>10s}'.format("Average segment length: ", str(int(round(mean(n_sum_len)))))
	print_raw_summary(raw_reg_sum)
	line(70)

def common_summary(cond_segments):
	cnd_reg_sum = {}
	c = 1
	for cnd in cond_segments:
		cnd_reg_sum['condition_' + str(c)] = cnd.count()
		c += 1
	return cnd_reg_sum

def raw_summary(cnd):
	raw_reg_sum = {}
	raw_reg_sum_nms = {}
	i = 1
	for c in cnd:
		raw_reg_sum['condition_' + str(i)]     = [len(cc) for cc in c]
		raw_reg_sum_nms['condition_' + str(i)] = [cc.fn for cc in c]
		i += 1
	return {'names': raw_reg_sum_nms, 'summary': raw_reg_sum}

def print_raw_summary(raw_sum):
	nms = raw_sum['names']
	vls = raw_sum['summary']
	cmn = raw_sum['common']
	kk = nms.keys()
	kk.sort()
	for cnd in kk:	
		line(70)
		print cnd
		line(70)
		for i in range(len(nms[cnd])):
			print '{:20s}{:>10s}'.format(nms[cnd][i], str(vls[cnd][i]))
		line(70)
		print '{:20s}{:>10s}'.format('Consensus regions: ', str(cmn[cnd]))
	
def mean(x):
	return float(sum(x))/len(x)

def line(l=70):
	print "="*l

def nfo(msg, field_size = 40):
	field = '{:' + str(field_size) + 's}'
	p.write(field.format(msg))



def make_filename(fn, signature, ts):
	basename = splitext(fn)[0]
	new_fn = basename + '_' + signature + '_' + ts + '.brf'
	return new_fn
	
def timestamp():
	ts = datetime.datetime.now().strftime("%d%m%Y%I%M%S%f")
	return ts

def done():
	print '[ DONE ]'

def read_bed2list(bed_files):
	'''
	read bed files into list using pybedtools
	'''
	bfiles = []
	for bfl in bed_files:
		bfiles.append(pybedtools.BedTool(bfl))
	return bfiles
	
def get_intersections(bed_files, min_n = -9):
	'''
	bed_files - output from read_bed2list
	min_n     - number of replicates per intersection
	'''
	tmp = pybedtools.BedTool()
	res_tmp = tmp.multi_intersect(i = [f.fn for f in bed_files])
	if min_n == -9:
		min_n = len(bed_files)
	if not min_n == 0:
		res_tmp = res_tmp.filter(n_common_filter, min_n)
	res_tmp = str(res_tmp)
	res = pybedtools.BedTool(res_tmp, from_string = True)
	return res

def n_common_filter(x, n):
	return int(x[3]) >= n

def rid_dict(max_id):
	srid = {}
	for id in range(1, max_id + 1):
		reg = 'RID_{:0>8}'.format(id)
		srid[reg] = 1
	return srid

def add_seq_length(regions):
	res = []
	for r in regions:
		#tmp = r[0:10]
		tmp = r[0:7]
		tmp.append(len(r))
		res.append(tmp)
	res = pybedtools.BedTool(res)
	return res

def add_index_segments(regions, max_id):
	res = []
	srid = rid_dict(max_id)
	for r in regions:
		tmp = r[0:5]
		#tmp = r[0:8]
		seg_id = srid[r[3]]
		srid[r[3]] += 1
		uniq_id = r[3] + '.' + str(seg_id)
		tmp.append(seg_id)
		tmp.append(uniq_id)
		res.append(tmp)
	res = pybedtools.BedTool(res)
	return res

# function not used !
def add_region_len_and_coords(regions):
	res = []
	for r in regions:
		tmp = r[0:4]
		# add region length
		tmp.append(len(r))
		# add region coords
		tmp.extend(r[0:3])
		res.append(tmp)
	res = pybedtools.BedTool(res)
	return res

def add_region_length(regions):
	res = []
	for r in regions:
		tmp = r[0:4]
		tmp.append(len(r))
		res.append(tmp)
	res = pybedtools.BedTool(res)
	return res

def remove_reg_coords(regions):
	res = []
	for r in regions:
		res.append(r[0:5])
	res = pybedtools.BedTool(res)
	return res

def add_indexes(regions):
	res = []
	id = 1
	for r in regions:
		tmp = r[0:3]
		hrid = 'RID_{:0>8}'.format(id)
		tmp.append(hrid)
		res.append(tmp)
		id += 1
	id -= 1
	res = pybedtools.BedTool(res)
	return [id, res]

def get_files(fl):
	cfiles = fl.split(';')
	dd = []
	for ff in cfiles:
		tmp = ff.split(',')
		dd.append(tmp)
	return dd
	


def getoptions(argv):
	options = {'help': False,
		   'inputfile': -1, 
		   'outputfile': -1,
		   'n_replicates': -9,
		   'n_conditions': 0,
		   'regions': False
		 }
	missing = False
	version = 'Version 0.0.0.2, July 2016'
	author  = 'Konrad J. Debski'
	arg_nfo = 'makeRegions.py\n -i <rep1_cond1,rep2_cond1,rep,rep3_cond1;rep1_cond2,rep_2,cond2,rep3_cond2>\n -o <output>\n -r <n_replicates - default all>\n -c <n_conditions - default 0>'
	changes = '- added regions coordinates\n - added argument checking'
	
	try:
		opts, args = getopt.getopt(argv, "hi:o:r:c:g", ["help", "input", "output", "n_rep", "n_cond", "regions"])
	except getopt.GetoptError:
		print 'Wrong options'
		print arg_nfo
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			print version
			print author
			print arg_nfo
			print changes
			sys.exit(2)
		if opt in ("-i", "--input"):
			options["inputfile"] = arg
		elif opt in ("-r", "--n_rep"):
			options["n_replicates"] = int(arg)
		elif opt in ("-c", "--n_cond"):
			options["n_conditions"] = int(arg)
		elif opt in ("-o", "--outputfile"):
			options["outputfile"] = arg
		elif opt in ("-g", "--regions"):
			options["regions"] = True
		
	if options["inputfile"] == -1:
		print 'Missing inputfile!'
		missing = True
	if options["outputfile"] == -1:
		print 'Missing outputfile!'
		missing = True
	if missing:
		sys.exit(2)

	return options

if __name__ == "__main__":
	main(sys.argv[1:])
