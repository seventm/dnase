#!/usr/bin/python

# import libraries
import sys
import re
import getopt
#deal with bam files
import pysam
# progress bar
from tqdm import tqdm

# deal with pipe IOError
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE,SIG_DFL) 

def main(argv):
	options = getoptions(argv)
		
	inputfiles = get_files(options['inputfile'])
	infiles = []
	innames = []
	for ff in inputfiles:
		try:
			infiles.append(pysam.AlignmentFile(ff[1]))
			innames.append(ff[0])
		except IOError as e:
			print "Unable to open file: " + ff[1]
			sys.exit(1)
	
	if not options['segments'] == -1:
		if options['segments'] == 'stdin':
			segfl = sys.stdin
		else:
			try:
				segfl = open(options['segments'], 'r')
			except IOError as e:
				print "Unable to open file: " + options['segments']
				sys.exit(2)
	
	if not options['regions'] == -1:
		if options['regions'] == 'stdin':
			regfl = sys.stdin
		else:
			try:
				regfl = open(options['regions'], 'r')
			except IOError as e:
				print "Unable to open file: " + options['regions']
				sys.exit(2)
	# prepare outputnames
	outputfile_seg = 'SEGMENTS_COUNTS_' + options['outputfile'] + '.scmx'
	outputfile_reg = 'REGIONS_COUNTS_'  + options['outputfile'] + '.rcmx'
	
	if not options['segments'] == -1:
		try:
			outfl_seg = open(outputfile_seg, 'w')
		except IOError as e:
			print "Unable to open file: " + outputfile_seg
			sys.exit(2)
	
	if not options['regions'] == -1:
		try:
			outfl_reg = open(outputfile_reg, 'w')
		except IOError as e:
			print "Unable to open file: " + outputfile_reg
			sys.exit(2)

	# create header for output matrix
	fields_names_seg = ['Region_ID', 'Region_length', 'Segment_ID', 'Unique_ID', 'Segment_length']
	fields_names_reg = ['Region_ID', 'Region_length']
	header_seg = ['#chr', 'segment_start', 'segment_end', '\t'.join(fields_names_seg),'\t'.join(innames)]
	header_reg = ['#chr', 'segment_start', 'segment_end', '\t'.join(fields_names_reg),'\t'.join(innames)]
	
	if not options['segments'] == -1:
		outfl_seg.write('\t'.join(header_seg) + '\n')
	if not options['regions'] == -1:
		outfl_reg.write('\t'.join(header_reg) + '\n')
	# disable progress bar if stdout / pipe
	#bar_disable = options['outputfile'] == -1
	bar_disable = False

	# for each segment in segment.bed file
	if not options['segments'] == -1:
		for seg in tqdm(segfl.readlines(), disable = bar_disable):
			# obtain segment coordinates
			fields    = seg.rstrip('\n')
			fields    = fields.split('\t')
			seg_ref   = fields[0]
			seg_start = int(fields[1])
			seg_end   = int(fields[2])
			cuts = []
			# for each bam file count number of DNase cuts in segment
			for infl in infiles:
				sample_cut = 0
				# for reads overlaping segments
				for sam_line in infl.fetch(seg_ref, seg_start, seg_end):
					# obtain cut positions for reads
					cut_pos = get_cut(sam_line, options['end'])
					# count DNase cuts if are inside the segment
					if cut_pos > seg_start and cut_pos < seg_end:
						sample_cut += 1
				# list of cuts for each sample / bam file
				cuts.append(sample_cut)
			segment = [seg_ref, str(seg_start), str(seg_end),  '\t'.join(fields[3:8]), '\t'.join(map(str, cuts))]
			segment = '\t'.join(segment)
			# save segment
			outfl_seg.write(segment + '\n')
	
	if not options['regions'] == -1:
		for reg in tqdm(regfl.readlines(), disable = bar_disable):
			# obtain regions coordiantes
			fields    = reg.rstrip('\n')
			fields    = fields.split('\t')
			reg_ref   = fields[0]
			reg_start = int(fields[1])
			reg_end   = int(fields[2])
			cuts = []
			# for each bam file count number of DNase cuts in region
			for infl in infiles:
				sample_cut = 0
				# for reads overlapping regions
				for sam_line in infl.fetch(reg_ref, reg_start, reg_end):
					# obtain cut positions for reads
					cut_pos = get_cut(sam_line, options['end'])
					# count DNase cuts if are inside the segment
					if cut_pos > reg_start and cut_pos < reg_end:
						sample_cut += 1
				# list of cuts for each sample / bam file
				cuts.append(sample_cut)
			region = [reg_ref, str(reg_start), str(reg_end), '\t'.join(fields[3:5]), '\t'.join(map(str, cuts))]
			region = '\t'.join(region)
			# save region
			outfl_reg.write(region + '\n')

	# close output file if not stdout
	if not options['segments'] == -1:
		outfl_seg.close()
	if not options['regions'] == -1:
		outfl_reg.close()

	# close each bam file
	for infl in infiles:
		infl.close()
# obtain sample_name:filename pairs from argument
def get_files(fl):
	fl = fl.split(',')
	dd = []
	for ff in fl:
		sample_name, filename = ff.split(':')
		dd.append((sample_name, filename))
	return dd

		
# return the position of dnase cut / 3 or 5 end of read
def get_cut(sam_line, end_mode):
	start = int(sam_line.reference_start)
	end = int(sam_line.reference_end)
	flag = str(sam_line.flag)
	if flag in ("0", "16"):
		position = start -1
		if end_mode == '3':
			if flag == "0":
				#position = end - 1
				position = end + 1
		elif end_mode == '5':
			if flag == "16":
				#position = end - 1
				position = end + 1
	return position

def getoptions(argv):
	options = {'help': False,
		   'inputfile': -1, 
		   'outputfile': -1,
		   'segments': -1,
		   'regions': -1,
		   'end':-1
		}
	missing = False
	version = 'Version 0.0.3.0, July 2016'
	author = 'Konrad J. Debski'
	arg_nfo = 'countDNaseCuts.py -i <sample_name1:inputfile1.bam,sample_name2:inputfile2.bam> -o <outputfile> -e <3 or 5> -s <segments.bed> -r <regions.bed>'
	changes = '- get_cut changed - fixed cutting point'

	try:
		opts, args = getopt.getopt(argv, "hi:o:e:s:r:", ["help", "input", "output", "end", "segments", "regions"])
	except getopt.GetoptError:
		print 'Wrong options'
		print arg_nfo
		sys.exit(2)

	if len(opts) == 0:
		print 'Missing options'
		print arg_nfo
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			print version
			print author
			print arg_nfo
			print changes
			sys.exit(2)
		elif opt in ("-i", "--input"):
			options['inputfile'] = arg
		elif opt in ("-s", "--segments"):
			options['segments'] = arg
		elif opt in ("-r", "--regions"):
			options['regions'] = arg
		elif opt in ("-e", "--end"):
			if arg in ("5", "3"):
				options['end'] = arg
			else:
				print 'Unknown end - 3 or 5 is possible'
				sys.exit(2)
		elif opt in ("-o", "--output"):
			if not arg == "stdout":
				options['outputfile'] = arg
			if arg == "stdout":
				print 'Output must be saved to the file!'
				sys.exit(2)
				

	if options['regions'] == 'stdin' and options['segments'] == 'stdin':
		print 'Regions and segments cannot be simultanously obtained from stdin!'
		missing = True
	if options['regions'] == -1 and options['segments'] == -1:
		print 'You must provide bed file with regions or and with segments!'
		missing = True
	if options['inputfile'] == -1:
		print 'Missing input files!'
		missing = True
	if options['outputfile'] == -1:
		print 'Missing output file!'
		missing = True
	if options['end'] == -1:
		print 'Cut end is not defined: 5 or 3 !'
		missing = True

	if missing:
		sys.exit(2)
	
	return(options)	


if __name__ == "__main__":
	main(sys.argv[1:])
	
