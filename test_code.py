import pybedtools


def read_hotspots(hotspots_files):
	hot_files = []
	for h_file in hotspots_files:
		hot_files.append(pybedtools.BedTool(h_file))
	return hot_files

def get_intersections(hot_files, min_hot = -9):
	'''
	hot_files - output from read_hotspots
	min_n - number of replicates per hotspot fragment
	'''
	tmp = pybedtools.BedTool()
	res_tmp = tmp.multi_intersect(i=[f.fn for f in hot_files])
	if min_hot == -9:
		min_hot = len(hot_files)
	if not min_hot == 0:
		res_tmp = res_tmp.filter(n_common_filter, min_hot)
	res_tmp = str(res_tmp)
	res = pybedtools.BedTool(res_tmp, from_string = True)
	return res

def n_common_filter(x, n):
	return int(x[3]) >= n

def add_indexes(regions):
	res = []
	id = 1 
	for r in regions:
		tmp = r[0:3]
		hrid = 'HRID_{:0>8}'.format(id)
		tmp.append(hrid)
		res.append(tmp)
		id += 1
	res = pybedtools.BedTool(res)
	return res
	
ht_files   = read_hotspots(['a.bed', 'b.bed', 'c.bed'])
ht_inter   = get_intersections(ht_files, 2)
ht_regions = ht_inter.merge()
ht_regions = add_indexes(ht_regions)

