library(edgeR)
library(jsonlite)


dnaseRegions <- setRefClass("dnaseRegions",
                            fields = list(
                              # input data // files
                              filename              = "character",
                              filesamples           = "character",
                              filecoverage          = "character",
                              filereads             = "character",
                              fileReadyRegions      = "character",
                              output                = "character",
                              isFilteredByCPM       = "logical",
                              isFilteredByCPMperBase= "logical",
                              isFilteredByRegLen    = "logical",
                              reads                 = "character",
                              timestamp             = "character",
                              edger_results         = "list",
                              # regions data
                              regions               = "matrix",
                              regions_id            = "character",
                              coords                = "data.frame",
                              reg_length            = "integer",
                              n_regions             = "integer",
                              n_samples             = "integer",
                              sample_names          = "character",
                              group_names           = "character",
                              groups                = "list",
                              cpm                   = "matrix",
                              cpmpb                 = "matrix",
                              coverage              = "list",
                              contrasts             = "list",
                              mean_cpm              = "matrix",
                              # filtering data
                              above_cpm             = "matrix",
                              above_cpm_groups      = "matrix",
                              any_above_cpm         = "logical",
                              above_cpmpb             = "matrix",
                              above_cpmpb_groups      = "matrix",
                              any_above_cpmpb         = "logical",
                              session               = "json",
                              # parameters
                              cpm_threshold         = "numeric",
                              cpm_min_samples       = "numeric",
                              cpmpb_threshold       = "numeric",
                              cpmpb_min_samples     = "numeric",
                              resize_factor         = "list",
                              ready2go              = "logical",
                              min_length            = "numeric",
                              max_length            = "numeric",
                              filtered_length       = "numeric",
                              filtered_cpm          = "numeric",
                              filtered_cpmpb        = "numeric",
                              n_start               = "numeric"
                            ),
                            methods = list(
                              params = function(){
                                is.empty <- function(x) length(x) == 0
                                list(
                                  'TS:'                       = timestamp,
                                  'Regions filename:'         = filename,
                                  'Sample filename:'          = filesamples,
                                  'Coverage filename:'        = filecoverage,
                                  'N-regions[start]:'         = n_start,
                                  'N-regions[cpm_filter]:'    = ifelse(is.empty(filtered_cpm),
                                                                       'Not used',
                                                                       as.character(filtered_cpm)),
                                  'N-regions[cpmpb_filter]:'  = ifelse(is.empty(filtered_cpmpb),
                                                                       'Not used',
                                                                       as.character(filtered_cpmpb)),
                                  'N-regions[length_filter]:' = ifelse(is.empty(filtered_length),
                                                                       'Not used',
                                                                       as.character(filtered_length)),
                                  'N-regions[tested]:'        = n_regions,
                                  'N-samples:'                = n_samples,
                                  'Min length [filter]:'      = ifelse(is.empty(min_length),
                                                                       'Undefined',
                                                                       as.character(min_length)),
                                  'Max length [filter]:'      = ifelse(is.empty(max_length),
                                                                       'Undefined',
                                                                       as.character(max_length)),
                                  'CPM cut-off:'              = ifelse(is.empty(cpm_threshold),
                                                                        'Undefined',
                                                                        as.character(cpm_threshold)),                               
                                  'Min samples in group [CPM]:'     = ifelse(is.empty(cpm_min_samples),
                                                                'Undefined',
                                                                as.character(cpm_min_samples)),
                                  'CPM per Base cut-off:'     = ifelse(is.empty(cpmpb_threshold),
                                                                        'Undefined',
                                                                        as.character(cpmpb_threshold)),                               
                                  'Min samples in group [CPM per base]:'     = ifelse(is.empty(cpmpb_min_samples),
                                                                'Undefined',
                                                                as.character(cpmpb_min_samples))
                             
                                  
                                )
                              },
                              readRegions = function(fn){
                                "Read dnase regions file."
                                if(!grepl('\\.rcmx$', fn)) 
                                  stop('Wrong file extension!')
                                
                                filename     <<- fn
                                isFilteredByCPM <<- FALSE
                                isFilteredByCPMperBase <<- FALSE
                                isFilteredByRegLen <<- FALSE
                                ready2go <<- FALSE
                                tmp               <- read.csv(fn, stringsAsFactors = FALSE, sep='\t')
                                
                                regions           <<- as.matrix(tmp[6:ncol(tmp)])
                                regions_id        <<- tmp[, 'Region_ID']
                                rownames(regions) <<- regions_id
                                sample_names      <<- colnames(tmp)[6:ncol(tmp)]
                                n_regions         <<- as.integer(nrow(regions))
                                n_start           <<- n_regions
                                n_samples         <<- as.integer(ncol(regions))
                                
                                coords            <<- tmp[, 1:3]
                                colnames(coords)  <<- c('chr', 'start', 'end')
                                rownames(coords)  <<- regions_id
                                
                                reg_length        <<- tmp[, 'Region_length']
                                names(reg_length) <<- regions_id
                                groups            <<- list()
                                group_names       <<- character()
                              },
                              readSegments = function(fn){
								"Read dnase segments file."
								if(!grepl('\\.scmx$', fn)) 
								    stop('Wrong file extension!')
								                    
								filename     <<- fn
								isFilteredByCPM <<- FALSE
								isFilteredByCPMperBase <<- FALSE
								isFilteredByRegLen <<- FALSE
								ready2go <<- FALSE
								tmp               <- read.csv(fn, stringsAsFactors = FALSE, sep='\t')
								                    
								regions           <<- as.matrix(tmp[9:ncol(tmp)])
								regions_id        <<- tmp[, 'Unique_ID']
								rownames(regions) <<- regions_id
								sample_names      <<- colnames(tmp)[9:ncol(tmp)]
								n_regions         <<- as.integer(nrow(regions))
								n_start           <<- n_regions
								n_samples         <<- as.integer(ncol(regions))
								                   
								coords            <<- tmp[, 1:3]
								colnames(coords)  <<- c('chr', 'start', 'end')
								rownames(coords)  <<- regions_id
								                    
								reg_length        <<- tmp[, 'Segment_length']
								names(reg_length) <<- regions_id
								groups            <<- list()
								group_names       <<- character()
						      },
                              readCoverage = function(fn){
                                filecoverage <<- fn
                                covData <- read.csv(fn, col.names=c('Sample', 'Coverage'), 
                                                    stringsAsFactors = FALSE, header = FALSE)
                                coverage <<- lapply(setNames(covData[,1], covData[, 'Sample']), 
                                                    function(smp) subset(covData, Sample == smp)[, 'Coverage'])
                                resize_factor <<- lapply(coverage, function(cover) cover / 10^6)
                              },
                              readSampleInfo = function(fn){
                                filesamples <<- fn
                                tmp <- read.csv(fn, header = FALSE, col.names=c('Group', 'Samples'), stringsAsFactors = FALSE)
                                tmp <- setNames(strsplit(tmp[, 'Samples'], ';'), tmp[, 'Group'])
                                addGroups(names(tmp))
                                for(grp in names(tmp))
                                  samples2group(grp, tmp[[grp]])
                              },
                              readReadsInfo = function(fn){
                                tmp <- read.csv(fn, header = FALSE, col.names=c('Samples', 'Reads'), stringsAsFactors = FALSE)
                                tmp <- setNames(tmp[, 'Reads'], tmp[, 'Samples'])
                                if(length(setdiff(names(tmp), sample_names)) != 0)
                                  stop('Unknown samples!')
                                filereads <<- fn
                                reads <<- tmp
                              },
                              dim = function(){
                                cat('Number of regions:', n_regions, '\nNumber of samples:', n_samples, '\n')
                                invisible(c(regions = n_regions, samples = n_samples))
                              },
                              # group / sample methods
                              addGroups = function(groups){
                                if(any(groups %in% group_names))
                                  stop('Group already exists!')
                                group_names <<- c(group_names, groups)
                              },
                              samples2group = function(group, sample){
                                if(!group %in% group_names)
                                  stop(paste0('Unknown group: ', group))
                                groups[[group]] <<- c(groups[[group]], sample)
                              },
                              getGroupAndSamples = function(){
                                list2namedVector <- function(lst){
                                  unlist(lapply(names(lst), 
                                                function(grp) 
                                                  setNames(lst[[grp]], 
                                                           rep(grp, length(lst[[grp]]))
                                                  )
                                  )
                                  )
                                }
                                list2namedVector(groups)
                              },
                              # cpm
                              calcCPM = function(){
                                cpm <<- regions
                                for(samp in sample_names)
                                  cpm[, samp] <<- regions[, samp] / resize_factor[[samp]]
                                message('CPM - calculated')
                              },
                              calcCPMperBase = function(){
                                if(sum(base::dim(cpm)) == 0)
                                  stop('calcCPM first!')
                                cpmpb <<- cpm
                                for(samp in sample_names)
                                  cpmpb[, samp] <<- cpm[, samp] / reg_length
                                message('CPM per Base - calculated')
                              },
                              # filtering
                              filterByCPM = function(minSamp = 2, threshold = 1){
                                if(sum(base::dim(cpm)) == 0)
                                  stop('calcCPM first!')
                                if(isFilteredByCPM)
                                  stop('Data already filtered! - see params')
                                # save params
                                message('Numebr of regions before filtering:', n_regions)
                                n_input <- n_regions
                                cpm_threshold <<- threshold
                                cpm_min_samples <<- minSamp
                                
                                above_cpm <<- matrix(logical(0), nrow = nrow(regions), ncol = ncol(regions))
                                rownames(above_cpm) <<- regions_id
                                colnames(above_cpm) <<- sample_names
                                above_cpm <<- cpm > threshold
                                
                                above_grp_cpm <- lapply(groups, function(grp) rowSums(above_cpm[, grp]))
                                above_cpm_groups <<- do.call(cbind, above_grp_cpm)
                                any_above_cpm <<- rowSums(above_cpm_groups >= minSamp) > 0
                                region_names <- names(any_above_cpm)[any_above_cpm]
                                keepRegions(region_names)
                                isFilteredByCPM <<- TRUE
                                message('Numebr of regions after filtering:', n_regions)
                                filtered_cpm <<- n_input - n_regions
                              },
                              filterByCPMperBase = function(minSamp = 2, threshold = 0.1){
                                if(sum(base::dim(cpmpb)) == 0)
                                  stop('calcCPMperBase first!')
                                if(isFilteredByCPMperBase)
                                  stop('Data already filtered! - see params')
                                # save params
                                message('Numebr of regions before filtering:', n_regions)
                                n_input <- n_regions
                                cpmpb_threshold <<- threshold
                                cpmpb_min_samples <<- minSamp
                                
                                above_cpmpb <<- matrix(logical(0), nrow = nrow(regions), ncol = ncol(regions))
                                rownames(above_cpmpb) <<- regions_id
                                colnames(above_cpmpb) <<- sample_names
                                above_cpmpb <<- cpmpb > threshold
                                
                                above_grp_cpmpb <- lapply(groups, function(grp) rowSums(above_cpmpb[, grp]))
                                above_cpmpb_groups <<- do.call(cbind, above_grp_cpmpb)
                                any_above_cpmpb <<- rowSums(above_cpmpb_groups >= minSamp) > 0
                                region_names <- names(any_above_cpmpb)[any_above_cpmpb]
                                keepRegions(region_names)
                                isFilteredByCPMperBase <<- TRUE
                                message('Numebr of regions after filtering:', n_regions)
                                filtered_cpmpb <<- n_input - n_regions
                              },
                              filterByRegLen = function(min = NULL, max = NULL){
                                if(isFilteredByRegLen)
                                  stop('Data already filtered by region length - see params!')
                                if(is.null(min) & is.null(max))	
                                  stop('Missing thresholds!')
                                message('Numebr of regions before filtering:', n_regions)
                                tmp <- reg_length
                                if(!is.null(min))
                                  tmp <- tmp >= min
                                if(!is.null(max))
                                  tmp <- tmp <= max
                                keepRegions(names(tmp))
                                min_length <<- as.numeric(min)
                                max_length <<- as.numeric(max)
                                filtered_length   <<-  n_regions - nrow(regions)
                                isFilteredByRegLen <<- TRUE
                                message('Numebr of regions after filtering:', n_regions)
                              },
                              ready = function(){
                                ready2go <<- TRUE
                                timestamp <<- format(Sys.time(), "TS_%Y-%m-%d_%H-%M-%S")
                                fileReadyRegions <<- paste0(gsub('\\.rcmx$', '', filename), timestamp, '.bed')
                                grp_mean_cpm <- lapply(groups, function(grp) apply(cpm[, grp], 1, mean))
                                mean_cpm <<- do.call(cbind, grp_mean_cpm)
                                write.table(coords, sep='\t', row.names=FALSE, 
                                            col.names =FALSE, file=fileReadyRegions, 
                                            quote = FALSE)
                                message('Regions data ready for analysis!')
                              },
                              addConrasts = function(contrasts) contrasts <<- contrasts,
                              save = function(prefix){
                                dir.create(timestamp)
                                session_info2json <- function(si, onlyLoaded = TRUE){
                                  class(si) <- 'list'
                                  class(si[[1]]) <- 'list'
                                  if(onlyLoaded)
                                    si[[2]] <- subset(si[[2]], si[[2]][,2]=='*')
                                  toJSON(si, pretty = TRUE)
                                }
                                
                                session <<- session_info2json(devtools::session_info())
                                lapply(names(edger_results),
                                       function(nm){
                                         grps <- contrasts[[nm]]
                                         samps <- getGroupAndSamples()[names(getGroupAndSamples()) %in% grps]
                                         res <- edger_results[[nm]][['table']]
                                         res <- cbind(coords[rownames(res), ], 
                                                      res, 
                                                      mean_cpm[rownames(res), unlist(grps)], 
                                                      cpm[rownames(res), colnames(cpm)%in%samps])
                                         fn <- paste0('Results_', nm, '_', timestamp, '.csv')
                                         
                                         write.csv(res, file.path(timestamp, fn))
                                       }
                                  )
                                jparam <- toJSON(params(), pretty=TRUE)
                                write(jparam,  file.path(timestamp, paste0('Params_', timestamp, '.json')))
                                write(session, file.path(timestamp, paste0('Session_', timestamp, '.json')))
                                save.image(file.path(timestamp, paste('ANALYSIS_', timestamp, '.RData')))
                                message('Files saved into:', timestamp)
                               invisible()
                              },
                              keepRegions = function(regIds){
                                regions_id <<- regIds
                                regions    <<- subset(regions, rownames(regions) %in% regions_id)
                                coords     <<- subset(coords, rownames(coords) %in% regions_id)
                                n_regions  <<- nrow(regions)
                                reg_length <<- reg_length[regions_id]
				if(sum(base::dim(cpm))!=0)
                                  cpm <<- subset(cpm, rownames(cpm) %in% regions_id)
				if(sum(base::dim(above_cpm))!=0) {
                                  above_cpm <<- subset(above_cpm, rownames(above_cpm) %in% regions_id)
                                  any_above_cpm <<- any_above_cpm[regions_id]
                                  above_cpm_groups <<- subset(above_cpm_groups, rownames(above_cpm_groups) %in% regions_id)
                                }
				if(sum(base::dim(cpmpb))!=0)
                                  cpmpb <<- subset(cpmpb, rownames(cpmpb) %in% regions_id)
				if(sum(base::dim(above_cpmpb))!=0) {
                                  above_cpmpb <<- subset(above_cpmpb, rownames(above_cpmpb) %in% regions_id)
                                  any_above_cpmpb <<- any_above_cpmpb[regions_id]
                                  above_cpmpb_groups <<- subset(above_cpmpb_groups, rownames(above_cpmpb_groups) %in% regions_id)
                                }
                              },
                              # differentiall anaysis
                              edger = function(){
                                if(!ready2go)
                                  stop('Data is not ready! use $ready()')
                                doWeighting <- FALSE
                                bTagWise    <- TRUE
                                grps <- as.factor(names(getGroupAndSamples()))
                                x <- DGEList(counts   = regions, 
                                             group    = grps, 
                                             lib.size = unlist(coverage))
                                y <- calcNormFactors(x, 
                                                     method="TMM",
                                                     doWeighting = doWeighting)
                                y <-  estimateCommonDisp(y)
                                
                                design <- model.matrix(~0+grps)
                                colnames(design) <- levels(grps)
                                y <- estimateGLMCommonDisp(y, design)
                                if(bTagWise) {
                                  y <- estimateGLMTagwiseDisp(y, design)
                                }
                                resGLM <- glmFit(y, design)
                                contrast <- vapply(contrasts, 
                                                   function(x) paste0(x[['trt']], '-', x[['ref']]), 
                                                   'character')
                                edger_results <<- lapply(contrast, function(cts){
                                  contr <- makeContrasts(contrasts = cts, levels=design)
                                  resLRT <- glmLRT(resGLM, contrast = contr)
                                  topTags(resLRT, 
                                          n = n_regions, 
                                          adjust.method = "BH", 
                                          sort.by = "PValue", 
                                          p.value=1)
                                }
                                  
                                )
                              }
                          )
                  )
                            
                            